﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbInitializer : IDbInitializer
    {
        private readonly DataContext dataContext;

        public DbInitializer(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void InitializeDb()
        {
            var createdDataContext = dataContext.Database.EnsureCreated();
            if (createdDataContext)
            {
                dataContext.AddRange(FakeDataFactory.Employees);
                dataContext.SaveChanges();

                dataContext.AddRange(FakeDataFactory.Customers);
                dataContext.SaveChanges();
            }
        }
    }
}
