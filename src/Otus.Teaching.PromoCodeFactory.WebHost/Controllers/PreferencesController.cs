﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : Controller
    {
        private readonly IRepository<Preference> preferenceRepository;

        public PreferencesController(IRepository<Preference> preferenceRepository)
        {
            this.preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<PreferenceShortResponse>> GetPreferencesAsync()
        {
            var preferences = await preferenceRepository.GetAllAsync();
            var customerShortResponse = preferences.Select(x => new PreferenceShortResponse { Preference = x.Name}).ToList();
            return customerShortResponse;
        }
    }
}
