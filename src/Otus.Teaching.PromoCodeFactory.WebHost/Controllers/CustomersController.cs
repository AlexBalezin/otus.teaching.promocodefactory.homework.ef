﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<PromoCode> promoCodeRepository;
        private readonly IRepository<Preference> preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<PromoCode> promoCodeRepository, IRepository<Preference> preferenceRepository)
        {
            this.customerRepository = customerRepository;
            this.promoCodeRepository = promoCodeRepository;
            this.preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await customerRepository.GetAllAsync();
            var customerShortResponse = customers.Select(x => new CustomerShortResponse(x)).ToList();
            return customerShortResponse;
        }

        /// <summary>
        /// Получить клиента с промокодами
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await customerRepository.GetByIdAsync(id);
            var customerResponse = new CustomerResponse(customer);
            return customerResponse;
        }

        /// <summary>
        /// Создать нового клиента с предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferencesAll = await preferenceRepository.GetAllAsync();
            var preferencesId = preferencesAll.Select(x => x.Id).Intersect(request.PreferenceIds);
            var customer = new Customer()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferencesAll.Where(x => preferencesId.Contains(x.Id)).ToList()
            };
            await customerRepository.CreateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Обновить данные клиента с его предпочтениями
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return BadRequest("Сотрудник с указанным Id не существует");
            }

            if (request.FirstName != null) customer.FirstName = request.FirstName;
            if (request.LastName != null) customer.LastName = request.LastName;
            if (request.Email != null) customer.Email = request.Email;
            if (request.PreferenceIds != null)
            {
                var preferences = new List<Preference>();
                foreach (var preferenceId in request.PreferenceIds)
                    preferences.Add(await preferenceRepository.GetByIdAsync(preferenceId));
                customer.Preferences = preferences;
            };

            await customerRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить клиента с его промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await customerRepository.GetByIdAsync(id);
            if(customer == null)
            {
                return BadRequest("Сотрудник с указанным Id не существует");
            }

            await customerRepository.DeleteByIdAsync(id);

            return Ok();
        }
    }
}